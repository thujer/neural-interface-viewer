/**
 * WifiDataInterface
 * ---------------------------------
 * @file src.ino
 *
 * @brief Wifi Data Interface
 *
 * @author Tomas Hujer
 * @contact tomas@hujer.eu
 */

#include <Arduino.h>
#include <ArduinoOTA.h>


// App configuration
#define APP_VERSION             "0.1.1"

//#define DEBUG

#define TM_SAMPLING_DEFAULT     20            // [ms]
#define TM_SENDING_DEFAULT      200           // [ms]
#define SAMPLES_BUFFER_SIZE     2000
#define TEXT_BUFFER_SIZE        6000
#define SAMPLE_COUNT_MAX        1000

#define AVERAGE_BUFFER_SIZE     3

// Analog input selection
#define PIN_ANALOG_INPUT        36


// Capacitive touch pin
#ifdef T2
    #define PIN_TOUCH           T2
#else
    #define PIN_TOUCH           GPIO_NUM_2
#endif

// Websocket port
#define PORT_WEBSOCKET          1337

// Uncomment to allow count loop tick
//#define LOOP_COUNTER

// Comment to switch to sampling by time in loop
//#define SAMPLING_BY_INTERRUPT

#ifdef SAMPLING_BY_INTERRUPT
    // Timer setup
    // https://techtutorialsx.com/2017/10/07/esp32-arduino-timer-interrupts/
    // Regarding the prescaler, we have said in the introductory section that typically the frequency of the base signal
    // used by the ESP32 counters is 80 MHz (this is true for the FireBeetle board). This value is equal to 80 000 000 Hz,
    // which means means the signal would make the timer counter increment 80 000 000 times per second.
    //
    // Although we could make the calculations with this value to set the counter number for generating the interrupt,
    // we will take advantage of the prescaler to simplify it. Thus, if we divide this value by 80 (using 80 as the prescaler value),
    // we will get a signal with a 1 MHz frequency that will increment the timer counter 1 000 000 times per second.
    //
    // From the previous value, if we invert it, we know that the counter will be incremented at each microsecond.
    // And thus, using a prescaler of 80, when we call the function to set the counter value for generating the interrupt,
    // we will be specifying that value in microseconds.
    #define TIMER_PRESCALER         80      // Default [80]

    // Timer alarm setup
    // https://techtutorialsx.com/2017/10/07/esp32-arduino-timer-interrupts/
    // Regarding the second argument, remember that we set the prescaler in order for this to mean the number of microseconds
    // after which the interrupt should occur. So, for this example, we assume that we want to generate an interrupt each second,
    // and thus we pass the value of 1 000 000 microseconds, which is equal to 1 second.
    //
    // Important: Take in consideration that this value is specified in microseconds only if we specify the value 80 for the prescaler.
    // We can use different prescaler values and in that case we need to do the calculations to know when the counter will reach a certain value.
    #define TIMER_ALARM_AT          20000    // Time in ms (is prescale = 80)
#endif

// Wifi configuration
// Uncomment to use wifi like AP station
// Let commented to connect to your own wifi
#define WIFI_MODE_SOFT_AP

#ifdef WIFI_MODE_SOFT_AP
    const char *SSID =  "Neural-Interface";
    const char *PASSWORD =  "NeuralInterface";
#else
    const char *SSID =  "YourSSID"; // Change to Your wifi SSID !!!
    const char *PASSWORD =  "YourPassword"; // Change to Your wifi password !!!
#endif

// Uncomment line bellow if You wanna to experiment with mDNS
// #define MDNS_SUPPORT

#define OTA_SUPPORT

#include <ArduinoJson.h>

#include <WiFi.h>

#ifdef MDNS_SUPPORT
#include <ESPmDNS.h>
#endif

#include <WebSocketsServer.h>

#include <Countdown.h>


// Servers
WebSocketsServer webSocket = WebSocketsServer(PORT_WEBSOCKET);

// Timers
Countdown* timerSampling;
Countdown* timerSending;

#ifdef LOOP_COUNTER
    Countdown* timerLoopCounter;
    unsigned long loopCounter = 0;
#endif

enum INPUT_SOURCE {
    INPUT_SOURCE_ZERO = 0,
    INPUT_SOURCE_ANALOG,
    INPUT_SOURCE_DIGITAL,
    INPUT_SOURCE_TOUCH,
    INPUT_SOURCE_RANDOM,
};


// Default timings
unsigned int tmSampling = TM_SAMPLING_DEFAULT;
unsigned int tmSending = TM_SENDING_DEFAULT;
byte inputSource = INPUT_SOURCE_ANALOG;

// Buffer rotation indexes
unsigned int indexWrite;    // Index for writting
unsigned int indexRead;     // Index for reading
unsigned int inBuffer;      // Number of items in buffer

// Average buffer
unsigned int indexAverageWrite;    // Index for writting

float valueLast;
float valueAverage;
float valueAverageSum;

bool flagMeasureRunning;

// Text buffer to send via websocket
char textBuffer[TEXT_BUFFER_SIZE];

// Sample item structure
struct Sample {
    unsigned long timestamp;
    float value;
    float average;
    float myoElim;
};

// Samples buffer
Sample samples[SAMPLES_BUFFER_SIZE];

// Samples average buffer
float averageBuf[AVERAGE_BUFFER_SIZE];

#ifdef SAMPLING_BY_INTERRUPT
    // Timer pointer
    hw_timer_t * timer = NULL;

    portMUX_TYPE mux = portMUX_INITIALIZER_UNLOCKED;
#endif


/**
 * Send samples array by websocket to all clients
 */
void sendSamples() {

    Sample sample;
    unsigned int msgLength = 0;
    unsigned int count = 0;


    if(!inBuffer) {
        return;
    }

    msgLength += sprintf(textBuffer + msgLength, "[");

    #ifdef SAMPLING_BY_INTERRUPT
        portENTER_CRITICAL(&mux);
    #endif

    while(inBuffer) {
        count++;

        if(count >= SAMPLE_COUNT_MAX) {
            break;
        }

        if((msgLength + 30) > sizeof(textBuffer)) {
            Serial.println("Potentionaly textBuffer overflow !!!");
            break;
        }

        // Rotate read index
        indexRead %= SAMPLES_BUFFER_SIZE;

        // Get sample from buffer
        sample = samples[indexRead];
   
        // Move read index
        indexRead++;

        // Decrement number of items in buffer
        inBuffer--;

        if(count > 1) {
            // Add separator after first item
            msgLength += sprintf(textBuffer + msgLength, ",");
        }

        msgLength += sprintf(textBuffer + msgLength, "[%lu,%.2f,%.2f,%.2f]", sample.timestamp, sample.value, sample.average, sample.myoElim);
    };

    #ifdef DEBUG
        for(byte i=0; i<100; i++) {
            Serial.print(textBuffer[i]);
        }
        Serial.print("...\r\n");
    #endif

    #ifdef SAMPLING_BY_INTERRUPT
        portEXIT_CRITICAL(&mux);
    #endif

    msgLength += sprintf(textBuffer + msgLength, "]");

    //Serial.printf("Msg length: %d, InBuffer: %d\r\n", msgLength, inBuffer);

    // Send to all clients
    webSocket.broadcastTXT(textBuffer);
}


/**
 * Returns value indetified by commandToDetect from JSON structure in command
 * @param command Pointer to JSON string
 * @param commandToDetect String Value identifier
 * @return String value or empty when identifier not found
 */
String getValueFromJSON(String* command, String commandToDetect) {

    String value = "";

    int cmdPos = command->indexOf(commandToDetect);
    if(cmdPos > -1) {

        for(byte i=cmdPos; i < command->length(); i++) {
            if(command->charAt(i) == ':') {

                byte ii = i + 1;
                while((command->charAt(ii) != '}') &&
                      (command->charAt(ii) != '{') &&
                      (ii < command->length())) {

                    value += command->charAt(ii);
                    ii++;
                };

                break;
            }
        }
    }

    return value;
}


/**
 * Callback: receiving any WebSocket message
 */
void onWebSocketEvent(uint8_t clientNum, WStype_t type, uint8_t * payload, size_t length) {

    String command = String((char *) payload);

    // Figure out the type of WebSocket event
    switch(type) {

        // Client has disconnected
        case WStype_DISCONNECTED:
            Serial.printf("[%u] Disconnected!\n", clientNum);
            break;

        // New client has connected
        case WStype_CONNECTED: {
                IPAddress ip = webSocket.remoteIP(clientNum);
                Serial.printf("[%u] Connection from ", clientNum);
                Serial.println(ip.toString());

                String textBuffer = "{\"tmSampling\":";
                textBuffer += tmSampling;
                textBuffer += ",\"tmSending\":";
                textBuffer += tmSending;
                textBuffer += ",\"inputSource\":";
                textBuffer += inputSource;
                textBuffer += ",\"measure\":";
                textBuffer += flagMeasureRunning ? 1 : 0;
                textBuffer += "}";

                webSocket.broadcastTXT(textBuffer);
            }
            break;

        // Handle text messages from client
        case WStype_TEXT: {
                
                String value = "";

                if(command[0] == '{') {

                    // {"tmSampling":200}
                    value = getValueFromJSON(&command, "tmSampling");
                    if(value != "") {
                        tmSampling = command.toInt();
                        Serial.printf("Changing sampling rate to %d\r\n", tmSampling);
                    }

                    // {"tmSending":200}
                    value = getValueFromJSON(&command, "tmSending");
                    if(value != "") {
                        tmSending = command.toInt();
                        Serial.printf("Changing sending rate to %s\r\n", value.c_str());
                    }

                    // {"inputSource":1}
                    value = getValueFromJSON(&command, "inputSource");
                    if(value != "") {
                        Serial.printf("Value %c\r\n", value.charAt(0));
                        inputSource = value[0] - '0';
                        Serial.printf("Changing input source to %d\r\n", inputSource);
                    }

                    value = getValueFromJSON(&command, "restart");
                    if(value == "1") {
                        ESP.restart();
                    }

                    value = getValueFromJSON(&command, "stop");
                    if(value == "1") {
                        flagMeasureRunning = false;
                        Serial.printf("Switching measure process to %d", flagMeasureRunning);
                    }

                    value = getValueFromJSON(&command, "start");
                    if(value == "1") {
                        flagMeasureRunning = true;
                        Serial.printf("Switching measure process to %d", flagMeasureRunning);
                    }
                }
                
                // Print out raw message and do anything U want
                Serial.printf("[%u] Received text: %s\n", clientNum, payload);
            }
            break;

        // For everything else: do nothing
        case WStype_BIN:
        case WStype_ERROR:
        case WStype_FRAGMENT_TEXT_START:
        case WStype_FRAGMENT_BIN_START:
        case WStype_FRAGMENT:
        case WStype_FRAGMENT_FIN:
        default:
            break;
    }
}


/**
 * Add sample to rotation buffer
 */
#ifdef SAMPLING_BY_INTERRUPT
void IRAM_ATTR putSampleToBuf() {
#else
void putSampleToBuf() {
#endif

    if(!flagMeasureRunning) {
        return;
    }

    #ifdef SAMPLING_BY_INTERRUPT
        portENTER_CRITICAL_ISR(&mux);
    #endif

    if(inBuffer >= SAMPLES_BUFFER_SIZE) {
        Serial.println("buffer full !!!");
    } else {

        Sample sample;
        
        sample.timestamp = millis();
        
        switch(inputSource) {
            case INPUT_SOURCE_ZERO:
                sample.value = 0;
                break;

            case INPUT_SOURCE_ANALOG:
                sample.value = analogRead(PIN_ANALOG_INPUT);
                break;

            case INPUT_SOURCE_DIGITAL:
                sample.value = digitalRead(PIN_ANALOG_INPUT);
                break;

            case INPUT_SOURCE_TOUCH:
                sample.value = touchRead(PIN_TOUCH);                
                break;

            case INPUT_SOURCE_RANDOM:
                sample.value = random(4096);    // DEBUG
                break;
        }
        
        

        //Serial.printf("Value: %d\r\n", sample.value);

        if((sample.value < 0) || (sample.value > 4095)) {
            Serial.printf("Bad value cutted %f at %lu\r\n", sample.value, sample.timestamp);
            sample.value = 0;
        }
        
        // Rotate index forward
        indexAverageWrite %= AVERAGE_BUFFER_SIZE;
        averageBuf[indexAverageWrite] = sample.value;

        // Rotate indexLast back
        unsigned int indexLast = (indexAverageWrite > 0) ? (indexAverageWrite - 1) : (AVERAGE_BUFFER_SIZE - 1);

        float valueLast = averageBuf[indexLast];

        //Serial.printf("ixAW/ixLst: %d / %d, %.2f, %.2f\r\n", indexAverageWrite, indexLast, sample.value, valueLast);

        // Remove last value and add new value from sum
        valueAverageSum -= valueLast;
        valueAverageSum += sample.value;

        // Store values to sample structure
        sample.average = valueAverageSum / AVERAGE_BUFFER_SIZE;
        sample.myoElim = valueAverage - valueLast;

        indexAverageWrite++;
        
        indexWrite %= SAMPLES_BUFFER_SIZE;
        samples[indexWrite] = sample;
        indexWrite++;
        inBuffer++;
    }

    #ifdef SAMPLING_BY_INTERRUPT
        portEXIT_CRITICAL_ISR(&mux);
    #endif
}


#ifdef SAMPLING_BY_INTERRUPT
    void timerInit() {
        
        // Setup timer
        timer = timerBegin(1, TIMER_PRESCALER, true);

        // Attach interrupt routine
        timerAttachInterrupt(timer, &putSampleToBuf, true);

        // Setup timer alarm
        timerAlarmWrite(timer, TIMER_ALARM_AT, true);

        // Enable timer alarm
        timerAlarmEnable(timer);    
    }
#endif


void setup() {

    timerSampling = new Countdown();
    timerSending = new Countdown();

    #ifdef LOOP_COUNTER
        timerLoopCounter = new Countdown();
    #endif

    flagMeasureRunning = false;

    indexWrite = 0;
    indexRead = 0;
    inBuffer = 0;

    indexAverageWrite = 0;
    valueLast = 0;
    valueAverage = 0;
    valueAverageSum = 0;

    // Fill averageBuf by zeroes
    for(int i=0; i<AVERAGE_BUFFER_SIZE; i++) {
        averageBuf[i] = 0;
    }

    Serial.begin(115200);
    while(!Serial);

    timerSampling->set(TM_SAMPLING_DEFAULT);
    timerSending->set(TM_SENDING_DEFAULT);
    
    pinMode(PIN_ANALOG_INPUT, INPUT);

    // Switch off audio amplifier on M5Stack
    #ifdef M5STACK
        dacWrite(25,0);
    #endif
    
    #ifdef WIFI_MODE_SOFT_AP

        uint64_t chipid;
        char ssid[30];

        chipid = ESP.getEfuseMac();//The chip ID is essentially its MAC address(length: 6 bytes).
	    //Serial.printf("ESP32 Chip ID = %04X",(uint16_t)(chipid>>32)); //print High 2 bytes
	    sprintf(ssid, "%s-%08X\n", SSID, (uint32_t) chipid); //print Low 4bytes

        // Create access point
        WiFi.softAP(ssid, PASSWORD);

        // Print our IP address
        Serial.println();
        Serial.println("AP running");
        Serial.print("My IP address: ");
        Serial.println(WiFi.softAPIP());
    #else
        // Connect to access point
        WiFi.begin(SSID, PASSWORD);
  
        while (WiFi.status() != WL_CONNECTED) {
            delay(1000);
            Serial.println("Connecting to WiFi..");
        }
 
        Serial.println(WiFi.localIP());
    #endif

    #ifdef MDNS_SUPPORT
        // Experimental mDNS support
        
        const char* mdnsName = "esp32";

        MDNS.begin(mdnsName);                        // start the multicast domain name server
        Serial.print("mDNS responder started: http://");
        Serial.print(mdnsName);
        Serial.println(".local");

        Serial.printf("Hostname: %s\r\n", WiFi.getHostname());
    #endif

    #ifdef OTA_SUPPORT
        ArduinoOTA
            .onStart([]() {
                String type;
                if (ArduinoOTA.getCommand() == U_FLASH) {
                    // U_FLASH
                    type = "sketch";
                } else {
                    // U_SPIFFS
                    type = "filesystem";
                }

                // NOTE: if updating SPIFFS this would be the place to unmount SPIFFS using SPIFFS.end()
                Serial.println("Start updating " + type);
            })
            .onEnd([]() {
                Serial.println("\nEnd");
            })
            .onProgress([](unsigned int progress, unsigned int total) {
                Serial.printf("Progress: %u%%\r", (progress / (total / 100)));
            })
            .onError([](ota_error_t error) {
                Serial.printf("Error[%u]: ", error);
                if (error == OTA_AUTH_ERROR) Serial.println("Auth Failed");
                else if (error == OTA_BEGIN_ERROR) Serial.println("Begin Failed");
                else if (error == OTA_CONNECT_ERROR) Serial.println("Connect Failed");
                else if (error == OTA_RECEIVE_ERROR) Serial.println("Receive Failed");
                else if (error == OTA_END_ERROR) Serial.println("End Failed");
            });

        ArduinoOTA.begin();
    #endif

    // Start WebSocket server and assign callback
    webSocket.begin();
    webSocket.onEvent(onWebSocketEvent);

    #ifdef LOOP_COUNTER
        timerLoopCounter->set(1000);
    #endif

    #ifdef SAMPLING_BY_INTERRUPT
        timerInit();
    #endif
}


void loop() {

    #ifndef SAMPLING_BY_INTERRUPT
        if(timerSampling->timeout()) {
            timerSampling->set(tmSampling);

            putSampleToBuf();
        }
    #endif

    if(timerSending->timeout()) {
        timerSending->set(tmSending);

        // Send data to client
        sendSamples();
    }

    #ifdef LOOP_COUNTER
    loopCounter++;
    if(timerLoopCounter->timeout()) {
        Serial.printf("Loop counter: %lu\r\n", loopCounter);
        loopCounter = 0;
        timerLoopCounter->set(1000);
    }
    #endif

    // Look for and handle WebSocket data
    webSocket.loop();

    #ifdef OTA_SUPPORT
        ArduinoOTA.handle();
    #endif
}

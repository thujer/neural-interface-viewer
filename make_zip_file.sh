!/bin/bash
TARGET=/tmp/Neural_Interface_Viewer
TARGET_ZIP_FILE=neural_interface_viewer.zip

rmdir $TARGET --ignore-fail-on-non-empty

mkdir $TARGET
mkdir $TARGET/html

cp ./.vscode $TARGET/.vscode -r
cp ./src $TARGET/src -r
cp ./doc $TARGET/doc -r
cp ./lib $TARGET/lib -r
cp ./html/free $TARGET/html/ -r
cp ./README.md $TARGET/ -r
cp ./platformio.ini $TARGET/ -r

cd /tmp
zip -r -D $TARGET_ZIP_FILE ./Neural_Interface_Viewer/

echo
echo "You can find zip file in /tmp/$TARGET_ZIP_FILE"
echo

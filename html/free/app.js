
/**
 * Neural Viewer by Chart-JS graphs
 * Autor: Tomas Hujer
 * 
 * TODO: autoscale
 * https://stackoverflow.com/questions/45500392/chart-js-automatic-chosen-scale-value
 */

		
let urlTemplate = "ws://{IPV4}:1337/";

let valueCountMax = 500;

Chart.defaults.global.animation = {
    duration: 0
}

/*
function createConfig(details, data) {
    return {
        type: 'line',
        data: {
            labels: ['Day 1', 'Day 2', 'Day 3', 'Day 4', 'Day 5', 'Day 6'],
            datasets: [{
                label: 'steppedLine: ' + details.steppedLine,
                steppedLine: details.steppedLine,
                data: data,
                borderColor: details.color,
                fill: false,
            }]
        },
        options: {
            responsive: true,
            title: {
                display: true,
                text: details.label,
            }
        }
    };
}
*/


let configDefault = {
    type: 'line',
    data: {
        labels: [],
        datasets: [{
            label: 'Samples',
            backgroundColor: window.chartColors.red,
            borderColor: window.chartColors.red,
            data: [],
            fill: false,
        }, {
            label: 'Average',
            backgroundColor: window.chartColors.blue,
            borderColor: window.chartColors.blue,
            data: [],
            fill: false,
        }, {
            label: 'Myo',
            backgroundColor: window.chartColors.green,
            borderColor: window.chartColors.green,
            data: [],
            fill: false,
        }]
    },
    options: {
        autoscale: false,
        spanGaps: false,
        
        responsive: true,
        title: {
            display: true,
            text: 'Neural Interface Viewer'
        },/*
        tooltips: {
            mode: 'index',
            intersect: false,
        },
        hover: {
            mode: 'nearest',
            intersect: true
        },*/
        scales: {
            xAxes: [{
                display: true,
                scaleLabel: {
                    display: true,
                    labelString: 'Time'
                }
            }],
            yAxes: [{
                display: true,
                scaleLabel: {
                    display: false,
                    labelString: 'Amplitude'
                }
            }]
        },
        animation: {
              duration: 0
        },
    }
};

let configSample = configDefault;
//let configAverage = configDefault;
//let configMyo = configDefault;

//configSample.data.datasets[0] = 'Samples';
//configAverage.options.title = 'Average';
//configMyo.options.title = 'Myo';

function init() {

    console.log('init');

    window.canvasSample = new Chart(document.getElementById('canvas-sample').getContext('2d'), configSample);
    //window.canvasAverage = new Chart(document.getElementById('canvas-average').getContext('2d'), configAverage);
    //window.canvasMyo = new Chart(document.getElementById('canvas-myo').getContext('2d'), configMyo);

    // Connect to WebSocket server
    wsConnect(urlTemplate);
};


// Call this to connect to the WebSocket server
function wsConnect(urlTemplate) {

    // Get IPv4 from input element
    let ipv4 = document.getElementById('ipv4').value;

    // Replace template by current ipv4 in input field
    let url = urlTemplate.replace(/{IPV4}/gi, ipv4);

    console.log('wsConnect', url);
    
    // Connect to WebSocket server
    websocket = new WebSocket(url);
    
    // Assign callbacks
    websocket.onopen = function(evt) { onOpen(evt) };
    websocket.onclose = function(evt) { onClose(evt) };
    websocket.onmessage = function(evt) { onMessage(evt) };
    websocket.onerror = function(evt) { onError(evt) };
}


// Called when a WebSocket connection is established with the server
function onOpen(evt) {

    console.log("Connected");
    
    // Request data sending
    doSend("startDataSend");
}


// Called when the WebSocket connection is closed
function onClose(evt) {

    // Log disconnection state
    console.log("Disconnected");

    // Try to reconnect after a few seconds
    setTimeout(function() {
        wsConnect(urlTemplate)
    }, 2000);
}


// Called when a message is received from the server
function onMessage(evt) {

    // Print out our received message
    console.log("Received ", evt.data.length, 'items');

    // Update circle graphic with LED state
    switch(evt.data[0]) {

        case '[':

            if(evt.data) {

                let aData = JSON.parse(evt.data);

                for(let ix=0; ix < aData.length; ix++) {

                    let data = aData[ix];

                    let timestamp = data[0];
                    let valueSample = data[1];
                    let valueAverage = data[2];
                    let valueMioElim = data[3];

                    if (configSample.data.datasets.length > 0) {

                        if(configSample.data.labels.length > valueCountMax) {
                            configSample.data.labels.shift();
                        }
                                                
                        configSample.data.labels.push(timestamp);

                        configSample.data.datasets[0].data.push(valueSample)
                        configSample.data.datasets[1].data.push(valueAverage)
                        configSample.data.datasets[2].data.push(valueMioElim)

                        configSample.data.datasets.forEach(function(dataset) {
                            if(dataset.data.length > valueCountMax) {
                                dataset.data.shift();
                            }
                        });

                    }
                }

                if(aData.length) {
                    window.canvasSample.update();
                    //window.canvasAverage.update();
                    //window.canvasMyo.update();
                }
            }
            break;

        case '{':
            let oData = JSON.parse(evt.data);

            if(oData.tmSampling) {
                // Refresh received value of sampling rate to input
                let eTmSampling = document.getElementById('tmSampling');
                eTmSampling.value = oData.tmSampling;
            }

            if(oData.tmSending) {
                // Refresh received value of sending rate to input
                let eTmSending = document.getElementById('tmSending');
                eTmSending.value = oData.tmSending;
            }

            break;

        default:
            break;
    }
}

// Called when a WebSocket error occurs
function onError(evt) {
    console.log("ERROR: " + evt.data);
}

// Sends a message to the server (and prints it to the console)
function doSend(message) {
    console.log("Sending: " + message);
    websocket.send(message);
}

// Resize all graphs on window resize
// https://www.chartjs.org/docs/latest/general/responsive.html
// https://developer.mozilla.org/en-US/docs/Web/API/WindowEventHandlers/onbeforeprint
window.addEventListener("beforeprint", function () {
    for (var id in Chart.instances) {
        Chart.instances[id].resize();
    }
})

// Call the init function as soon as the page loads
window.addEventListener("load", init, false);

// Add button click event
document.getElementById('change-ipv4').addEventListener('click', function() {
    console.log('Doing IPv4 change...');
    websocket.close();
})

document.getElementById('change-sampling-rate').addEventListener('click', function() {
    let value = document.getElementById('tmSampling').value;
    console.log('Doing change of sampling rate to ' + value + '...');
    websocket.send('{"tmSampling":' + value + '}')
})

document.getElementById('change-sending-rate').addEventListener('click', function() {
    let value = document.getElementById('tmSending').value;
    console.log('Doing change of sending rate to ' + value + '...');
    websocket.send('{"tmSending":' + value + '}')
})


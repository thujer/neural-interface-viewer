
let lineArr = [];
let MAX_LENGTH = 200;
let duration = 500;

let chart = realTimeLineChart();

let urlTemplate = "ws://{IPV4}:1337/";

let valueCountMax = 1000;

let flagMeasure = false;
let flagRecordData = false;
let flagSimulateIncomingData = false;

let millisStart = 0;

let websocket = undefined;


/**
 * Init event
 */
function onInit() {
    checkConnection();
    setInterval(checkConnection, 2000);
    refreshDataLength();
    registerElementEvents();
};


/**
 * Test connection status, try to connect if not connected
 */
function checkConnection() {

    let eConnectionState = document.getElementById('connection-state');

    if((websocket === undefined) || (websocket.readyState !== WebSocket.OPEN)) {
        wsConnect(urlTemplate);
    }

    switch(websocket.readyState) {
        case WebSocket.CONNECTING: eConnectionState.innerHTML = 'CONNECTING'; break;
        case WebSocket.OPEN: eConnectionState.innerHTML = 'OPEN'; break;
        case WebSocket.CLOSED: eConnectionState.innerHTML = 'CLOSED'; break;
        case WebSocket.CLOSING: eConnectionState.innerHTML = 'CLOSING'; break;
    }
}


/**
 * Call this to connect to the WebSocket server
 * @param {*} urlTemplate 
 */
function wsConnect(urlTemplate) {

    // Get IPv4 from input element
    let ipv4 = document.getElementById('ipv4').value;

    // Replace template by current ipv4 in input field
    let url = urlTemplate.replace(/{IPV4}/gi, ipv4);

    console.log('wsConnect', url);

    // Connect to WebSocket server
    websocket = new WebSocket(url);

    // Assign callbacks
    websocket.onopen = function(evt) { onOpen(evt) };
    websocket.onclose = function(evt) { onClose(evt) };
    websocket.onmessage = function(evt) { onMessage(evt) };
    websocket.onerror = function(evt) { onError(evt) };
}


/**
 * Called when a WebSocket connection is established with the server
 * @param {*} evt 
 */
function onOpen(evt) {
    console.log("Connected");
}


/**
 * Called when the WebSocket connection is closed
 * @param {*} evt 
 */
function onClose(evt) {
    console.log("Disconnected");
}


/**
 * Called when a message is received from the server
 * @param {*} evt 
 */
function onMessage(evt) {

    // Print out our received message
    console.log("Received ", evt.data.length, 'items');

    // Update circle graphic with LED state
    switch (evt.data[0]) {

        case '[':

            if (evt.data) {

                let aData = JSON.parse(evt.data);

                for (let ix = 0; ix < aData.length; ix++) {

                    let data = aData[ix];

                    let timestamp = data[0];
                    let valueSample = data[1];
                    let valueAverage = data[2];
                    let valueMioElim = data[3];

                    var lineData = {
                        time: timestamp,
                        x: valueSample,
                        y: valueAverage,
                        z: valueMioElim,
                    };

                    lineArr.push(lineData);

                    if (lineArr.length > 30) {
                        lineArr.shift();
                    }
                }

                d3.select("#chart").datum(lineArr).call(chart);

                if(flagRecordData) {
                    // Unlike localStorage, you can store non-strings.
                    localforage.setItem(aData[0][0], aData)
                    .then(function(value) {
                        // This will output `1`.
                        console.log(value[0]);

                        refreshDataLength();
                    })
                    .catch(function(err) {
                        // This code runs if there were any errors
                        console.log(err);
                    });

                    
                }
            }
            break;

        case '{':
            let oData = JSON.parse(evt.data);

            if (oData.tmSampling) {
                // Refresh received value of sampling rate to input
                let eTmSampling = document.getElementById('tmSampling');
                eTmSampling.value = oData.tmSampling;
            }

            if (oData.tmSending) {
                // Refresh received value of sending rate to input
                let eTmSending = document.getElementById('tmSending');
                eTmSending.value = oData.tmSending;
            }

            if (oData.inputSource) {
                // Refresh received value of selected input
                let eChangeInput = document.getElementById('change-input');
                eChangeInput.value = oData.inputSource;
            }

            if (oData.measure) {
                // Refresh received measure state
                let measureButton = document.getElementById('measure');
                if(oData.measure) {
                    measureButton.classList.add('active');
                }
            }

            break;

        default:
            break;
    }
}


/**
 * Called when a WebSocket error occurs
 */
function onError(evt) {
    console.log("ERROR: " + evt.data);
}


/**
 * Called when window resized
 */
function onResize() {

    console.log('Resize')

    if (d3.select("#chart svg").empty()) {
        return;
    }

    chart.width(d3.select("#chart").style("width").replace(/(px)/g, ""));

    d3.select("#chart").call(chart);
}


/**
 * Called in page loaded
 */
function onLoad() {
    seedData();
    d3.select("#chart").datum(lineArr).call(chart);
    d3.select(window).on('resize', onResize);
}


/**
 * Push first data
 */
function seedData() {
    let now = new Date();
    for (let i = 0; i < MAX_LENGTH; ++i) {
        lineArr.push({
            time: 0,//new Date(now.getTime() - ((MAX_LENGTH - i) * duration)),
            x: 0,
            y: 0,
            z: 0,
        });
    }
}


/**
 * Export data from storage to excel file
 */
function exportData() {

    var tab_text = "<table>\r\n";

    tab_text += '<tr><th>Timestamp</th><th>RAW</th><th>Average</th><th>MIO</th></tr>\r\n';

    // The same code, but using ES6 Promises.
    localforage.iterate(function(value, key, iterationNumber) {

        // Resulting key/value pair -- this callback
        // will be executed for every item in the
        // database.

        for(let i = 1 ; i < value.length ; i++) {     
            tab_text += '<tr><td>' + value[i][0] + '</td><td>' + value[i][1] + '</td><td>' + value[i][2] + '</td><td>' + value[i][3] + '</td></tr>\r\n';
        }
    })
    .then(function() {

        console.log('Iteration has completed');

        tab_text = tab_text + "</table>\r\n";

        var uri = 'data:text/csv;charset=utf-8,' + tab_text;

        var downloadLink = document.createElement("a");
        downloadLink.href = uri;
        downloadLink.download = "data.xls";
        
        document.body.appendChild(downloadLink);
        downloadLink.click();
        document.body.removeChild(downloadLink);
    })
    .catch(function(err) {
        // This code runs if there were any errors
        console.log(err);
    });
}


/**
 * Update current data size value
 */
function refreshDataLength() {
    if(typeof(localforage) != 'undefined') {
        let eDataSize = document.getElementById('data-size');

        localforage.length()
        .then(function(length) {
            eDataSize.innerHTML = length;
        })
        .catch(function(e) {
            console.error(e);
        })
    }
}


/**
 * Create fake data message
 */
function getFakeData() {

    const millis = Date.now() - millisStart;
    let aData = [];

    for(let i=0; i<10; i++) {
        aData.push([Math.floor(millis), Math.round(Math.random() * 4096), Math.round(Math.random() * 4096), Math.round(Math.random() * 4096)]);
    }

    let oMessage = {
        data: JSON.stringify(aData),
    }

    return oMessage;
}


/**
 * Local simulated data input
 * @param flagStart Boolean true = start simulation, false = stop simulation
 */
function simulateIncomingData(flagStart) {

    // Start repeating when called with true
    if(flagStart === true) {
        millisStart = Date.now();
        flagSimulateIncomingData = true;
    }

    // Stop repeating when called with false
    if(flagStart === false) {
        flagSimulateIncomingData = false;
    }

    if(flagSimulateIncomingData === true) {
        onMessage(getFakeData());
        setTimeout(simulateIncomingData, 200 + Math.random() * 50);
    }
}


/**
 * Setup element events
 */
function registerElementEvents() {

    /**
     * Add ipv4 change button event
     */
    document.getElementById('change-ipv4').addEventListener('click', function() {
        console.log('Doing IPv4 change...');
        if(websocket.readyState === WebSocket.OPEN) {
            websocket.close();
        } else {
            alert('Websocket connection is not opened');
        }
    })


    /**
     * Add sampling rate change button event
     */
    document.getElementById('change-sampling-rate').addEventListener('click', function() {
        if(websocket.readyState === WebSocket.OPEN) {
            let value = document.getElementById('tmSampling').value;
            console.log('Doing change of sampling rate to ' + value + '...');
            websocket.send('{"tmSampling":' + value + '}')
        } else {
            alert('Websocket connection is not opened');
        }
    })


    /**
     * Add sending rate button change event
     */
    document.getElementById('change-sending-rate').addEventListener('click', function() {
        let value = document.getElementById('tmSending').value;
        console.log('Doing change of sending rate to ' + value + '...');

        if(websocket.readyState === WebSocket.OPEN) {
            websocket.send('{"tmSending":' + value + '}')
        } else {
            alert('Websocket connection is not opened');
        }
    })


    /**
     * Add input change selector event
     */
    document.getElementById('change-input').addEventListener('change', function() {
        console.log('Doing change input source to ' + this.value + '...');

        if(this.value === 'demo') {
            simulateIncomingData(true);
        } else {
            simulateIncomingData(false);

            if(websocket.readyState === WebSocket.OPEN) {
                websocket.send('{"inputSource":' + this.value + '}')
            } else {
                alert('Websocket connection is not opened');
            }
        }
    })


    /**
     * Add restart button event
     */
    document.getElementById('restart-chip').addEventListener('click', function() {
        console.log('Send restart request');

        if(websocket.readyState === WebSocket.OPEN) {
            websocket.send('{"restart":1}');
            websocket.close();
        } else {
            alert('Websocket connection is not opened');
        }
    })


    /**
     * Add clear data event
     */
    document.getElementById('clear-data').addEventListener('click', function() {
        console.log('Clearing data...');
        localforage.clear();

        refreshDataLength();
    })


    /**
     * Export data
     */
    document.getElementById('export-data').addEventListener('click', function() {
        console.log('Export data...');
        exportData();
    })


    /**
     * Add measure start/stop button event
     */
    document.getElementById('measure').addEventListener('click', function() {

        // When simulation
        if(flagSimulateIncomingData) {

            flagMeasure = !flagMeasure;

            if(flagMeasure) {
                this.innerHTML = 'Stop measure';
                this.classList.add('active');
            } else {
                this.innerHTML = 'Start measure';
                this.classList.remove('active');
            }

            return;
        }

        if(flagMeasure) {
            console.log('Requesting measure stop...');

            if(websocket.readyState === WebSocket.OPEN) {
                flagMeasure = false;
                websocket.send('{"stop":1}');
                this.innerHTML = 'Start measure';
                this.classList.remove('active');
            } else {
                alert('Websocket connection is not opened');
            }
        } else {
            console.log('Requesting measure start...');

            if(websocket.readyState === WebSocket.OPEN) {
                flagMeasure = true;
                websocket.send('{"start":1}');
                this.innerHTML = 'Stop measure';
                this.classList.add('active');
            } else {
                alert('Websocket connection is not opened');
            }
        }
    })


    /**
     * Add data record start/stop event
     */
    document.getElementById('record-data').addEventListener('click', function() {
        if(flagRecordData) {
            console.log('Stop record data...');
            flagRecordData = false;
            this.innerHTML = 'Start record data';
            this.classList.remove('active');
        } else {
            console.log('Start record data...');
            flagRecordData = true;
            this.innerHTML = 'Stop record data';
            this.classList.add('active');
        }
    })
}


// Call the init function as soon as the page loads
window.addEventListener("load", onInit, false);
window.addEventListener("load", onResize, false);
window.addEventListener("load", onLoad, false);

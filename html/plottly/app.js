
console.log('App start');

let urlTemplate = "ws://{IPV4}:1337/";

let valueCountMax = 200;


function initGraph() {
    
    var trace1 = {
        x: [],
        y: [],
        mode: 'lines',
        line: {
            color: '#80CAF6',
            shape: 'spline'
        }
    }
    
    var trace2 = {
        x: [],
        y: [],
        xaxis: 'x2',
        yaxis: 'y2',
        mode: 'lines',
        line: { color: '#DF56F1' }
    };

    var layout = {
        xaxis: {
            type: 'date',
            domain: [0, 1],
            showticklabels: false
        },
        yaxis: { domain: [0.6, 1] },
        xaxis2: {
            type: 'date',
            anchor: 'y2',
            domain: [0, 1]
        },
        yaxis2: {
            anchor: 'x2',
            domain: [0, 0.4]
        },
    }
    
    var data = [trace1, trace2];
    
    Plotly.plot('graph', data, layout);
}



// Call this to connect to the WebSocket server
function wsConnect(urlTemplate) {

    // Get IPv4 from input element
    let ipv4 = document.getElementById('ipv4').value;

    // Replace template by current ipv4 in input field
    let url = urlTemplate.replace(/{IPV4}/gi, ipv4);

    console.log('wsConnect', url);
    
    // Connect to WebSocket server
    websocket = new WebSocket(url);
    
    // Assign callbacks
    websocket.onopen = function(evt) { onOpen(evt) };
    websocket.onclose = function(evt) { onClose(evt) };
    websocket.onmessage = function(evt) { onMessage(evt) };
    websocket.onerror = function(evt) { onError(evt) };
}


// Called when a WebSocket connection is established with the server
function onOpen(evt) {

    console.log("Connected");
    
    // Request data sending
    doSend("startDataSend");
}


// Called when the WebSocket connection is closed
function onClose(evt) {

    // Log disconnection state
    console.log("Disconnected");

    // Try to reconnect after a few seconds
    setTimeout(function() {
        wsConnect(urlTemplate)
    }, 2000);
}


// Called when a message is received from the server
function onMessage(evt) {

    // Print out our received message
    console.log("Received ", evt.data.length, 'items');

    // Update circle graphic with LED state
    switch(evt.data[0]) {

        case '[':

            if(evt.data) {

                let aData = JSON.parse(evt.data);

                for(let ix=0; ix < aData.length; ix++) {

                    let data = aData[ix];

                    let timestamp = data[0];
                    let valueSample = data[1];
                    let valueAverage = data[2];
                    let valueMioElim = data[3];

                    var update = {
                        x: [[timestamp], [timestamp]],
                        y: [[valueAverage], [valueMioElim]]
                    }
                }

                // https://plotly.com/javascript/plotlyjs-function-reference/#plotlyextendtraces
                Plotly.extendTraces('graph', update, [0, 1], 100)
            }
            break;

        case '{':
            let oData = JSON.parse(evt.data);

            if(oData.tmSampling) {
                // Refresh received value of sampling rate to input
                let eTmSampling = document.getElementById('tmSampling');
                eTmSampling.value = oData.tmSampling;
            }

            if(oData.tmSending) {
                // Refresh received value of sending rate to input
                let eTmSending = document.getElementById('tmSending');
                eTmSending.value = oData.tmSending;
            }

            break;

        default:
            break;
    }
}

// Called when a WebSocket error occurs
function onError(evt) {
    console.log("ERROR: " + evt.data);
}

// Sends a message to the server (and prints it to the console)
function doSend(message) {
    console.log("Sending: " + message);
    websocket.send(message);
}

window.addEventListener("load", function() {

    initGraph();

    wsConnect(urlTemplate);
    
});



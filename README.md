
# Neural interface viewer

## Quick start Arduino IDE
- copy libraries from "lib" directory to Arduino libraries directory (for example ~/Arduino/libraries on Linux)
- Maybe You will need to move files in Countdown/src/* directory to one level up (when You'll have problem with compilation this library)
- Open Arduino IDE and open project subdirectory ./src
- Decide between SoftAP or Client wifi mode (see [Wifi mode selection](#wifi_mode_change) for more info) 
- If You decide to use client mode don't forgot to change wifi credentials in Wifi configuration section
```
...
#else
    const char *SSID =  "YourSSID"; // Change to Your wifi SSID !!!
    const char *PASSWORD =  "YourPassword"; // Change to Your wifi password !!!
#endif
...
```
- Start Serial monitor
- Build & Upload project
- Look at Serial monitor to get IPv4 address of device
- Open index.html in browser (or refresh) to connect
- If Your own AP used, then change IPv4 in input box and click on Change IPv4 button
- Remember that it will need to change IPv4 when it will be changed by your router
---
## Quick start Platformio plugin for VSCode/VSCodium

- install VSCode <https://platformio.org/install/ide?install=vscode>
- install Platformio <https://platformio.org/platformio-ide>
- clone project from repositoty ```git clone git@gitlab.com:thujer/neural-interface-viewer.git``` 
  or download it as zip archive and unpack <https://gitlab.com/thujer/neural-interface-viewer/-/archive/master/neural-interface-viewer-master.zip>
- Open VSCode open project directory
- If not installed Platformio plugin, then install it now in File/Settings/Extensions - search and install "Platformio IDE"
- Other platform and project libraries will be downloaded automatically
- Decide between SoftAP or Client wifi mode (see [Wifi mode selection](#wifi_mode_change) for more info)
- Change wifi credentials
```
// Wifi configuration
const char *ssid =  "YourWifiSSID";
const char *password =  "YourPassword";
```
- Just select Platformio icon, then Upload and Monitor

---
## How to build project via command line

Clone project from GIT repository
```
git clone git@gitlab.com:thujer/wifi-data-bridge.git
```

1. `Install PlatformIO Core <http://docs.platformio.org/page/core.html>`_
2. Download `development platform with examples <https://github.com/platformio/platform-espressif32/archive/develop.zip>`_
3. Extract ZIP archive
4. Run these commands in 

 bash

    # Change directory to project
    > cd wifi-data-bridge/

    # Build project
    > platformio run

    # Upload firmware
    > platformio run --target upload

    # Build specific environment
    > platformio run -e esp32dev

    # Upload firmware for the specific environment
    > platformio run -e esp32dev --target upload

    # Clean build files
    > platformio run --target clean

Library Dependency Finder must be "chain+"
https://docs.platformio.org/en/latest/projectconf/section_env_library.html#projectconf-lib-ldf-mode

Platformio UDEV rules
https://docs.platformio.org/en/latest/faq.html#platformio-udev-rules

---
## <a name="wifi_mode_change"></a>Wifi mode selection

There is a definition WIFI_MODE_SOFT_AP to conditional compilation of wifi mode. If this constant is defined, then SoftAP mode will be used, when it's undefined (commented out) client mode will be used. In both cases will be used values in ssid and password variables.

```
#define WIFI_MODE_SOFT_AP
```

```
  #ifdef WIFI_MODE_SOFT_AP
      // Create access point
      WiFi.softAP(ssid, password);

      // Print our IP address
      Serial.println();
      Serial.println("AP running");
      Serial.print("My IP address: ");
      Serial.println(WiFi.softAPIP());
  #else
      // Connect to access point
      WiFi.begin(ssid, password);

      while (WiFi.status() != WL_CONNECTED) {
          delay(1000);
          Serial.println("Connecting to WiFi..");
      }

      Serial.println(WiFi.localIP());
  #endif
```

![wifi mode selection](https://gitlab.com/thujer/neural-interface-viewer/-/raw/master/doc/wifi_mode_selection.png?inline=false "Wifi mode selection")

---
## Upload Over the air (OTA)

Change wifi mode to SoftAP

! In this mode You can't use serial monitor to debug !

Connect to ESP32 AP

Select wifi port to use OTA update as showed on picture bellow

If You can't see this port, try to restart Arduino IDE !

![Select port to OTA update](https://lastminuteengineers.com/wp-content/uploads/arduino/Select-OTA-Port-in-Arduino-IDE.png)

If You have any problem, please try to look on this original article:  [esp32-ota-updates-arduino-ide](https://lastminuteengineers.com/esp32-ota-updates-arduino-ide/)

---
## Inspiration

How to Create a Web Server (with WebSockets) Using an ESP32 in Arduino
<https://www.youtube.com/watch?v=mkXsmCgvy0k>
<https://shawnhymel.com/1882/how-to-create-a-web-server-with-websockets-using-an-esp32-in-arduino/>

How to get processing on Linux to communicate via rfcomm
<https://forum.processing.org/one/topic/how-to-get-processing-on-linux-to-communicate-via-rfcomm.html>

WebSocket Server and Client for Arduino
<https://github.com/Links2004/arduinoWebSockets>

How to plot on Android from Arduino using Bluetooth
<https://www.youtube.com/watch?v=-xlHUpQPdi8&feature=youtu.be>

Sample project of how to use the app with bluetooth module connected to arduino
<https://github.com/gthe856/arduinoBluetoothSample>

Debugging sensors on a microprocessor can be a hassle and the most used approach is to output the sensor values to a serial monitor
<https://github.com/sebnil/RealtimePlotter>

Arduino Processing and Bluetooth
<https://www.youtube.com/watch?v=VGJCj0Hr1vQ>

Processing for Android
<https://android.processing.org/tutorials/android_studio/index.html>

Beautiful HTML5 Charts & Graphs
<https://canvasjs.com>

OTA updates
<https://lastminuteengineers.com/esp32-ota-updates-arduino-ide>

LocalForage
https://localforage.github.io/localForage/#data-api-length

---
## Author
Tomas Hujer

Web & IoT developer

Mailto: tomas@hujer.eu

Web: tomas.hujer.eu

